Learning journal unit 6
-----------------------

- qa.py (string and list operations)
- qb.py (list examples)

Peer assessment
---------------

In terms of peer assessment received it is a bit better (compared to before). Feedback seems to be according to solutions provided by course instructor. Also, feedback seems to be given by 3 peers (compared to 0,1 or 2 as before). In terms of peer assessment given, no major changes. I tend to look for solutions provided by course instructor. If peer solutions match, feedback is usually positive according to grading guide. If peer solutions do not match solutions as provided by course instructor and if test cases are not correct, feedback can be negative, again according to grading guide.
