#!/usr/bin/python

"""
FILE qb.py

list examples

OUTPUT

---> nested list
[['vegetable', 'lettuce'], ['fruit', 'lemmon'], ['fruit', 'oranges'], ['fruit', 'bananas']]
---> duplicate list
['butter', 'flour', 'yeast', 'sugar', 'butter', 'flour', 'yeast', 'sugar']
---> reverse list using slices
[['fruit', 'bananas'], ['fruit', 'oranges'], ['fruit', 'lemmon'], ['vegetable', 'lettuce']]
---> extending list
['butter', 'flour', 'yeast', 'sugar', 'butter', 'flour', 'yeast', 'sugar', 'eggs']
---> a list filter to remove vegetables
[['fruit', 'lemmon'], ['fruit', 'oranges'], ['fruit', 'bananas']]
---> an unexpected thing, print index of item that does not exist
Traceback (most recent call last):
  File "./qb.py", line 56, in <module>
    print(lstb.index("cream"))
ValueError: 'cream' is not in list

"""

def filter_vegetables(item):
    """
        method filter_vegetables

        remove vegetables from list

        parameters

        item(list) a list that has couple of elements, type and name

        return

        check (boolean) False if item is a vegetable True in other case
    """
    if item[0] == "vegetable":
        return False
    else:
        return True

if __name__ == "__main__":
    nstdlsta = [["vegetable","lettuce"],["fruit","lemmon"],["fruit","oranges"],["fruit","bananas"]]
    nstdlstb = None
    lsta = ["butter","flour","yeast","sugar"]
    lstb = None

    print("---> nested list")
    print(nstdlsta)
    print("---> duplicate list")
    lstb = lsta * 2
    print(lstb)
    print("---> reverse list using slices")
    print(nstdlsta[::-1])
    print("---> extending list")
    lstb += ["eggs"]
    print(lstb)
    print("---> a list filter to remove vegetables")
    nstdlstb = filter(filter_vegetables, nstdlsta)
    print(nstdlstb)
    print("---> an unexpected thing, print index of item that does not exist")
    """
        thought there is some return value to indicate item does not exist
        
        instead there is an error
    """
    print(lstb.index("cream"))
