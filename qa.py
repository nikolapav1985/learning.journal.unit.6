#!/usr/bin/python

"""
FILE qa.py

string and list examples of operations

OUTPUT

---> original string
apples oranges grapes bananas melons peaches pears
---> string split into a list
['apples', 'oranges', 'grapes', 'bananas', 'melons', 'peaches', 'pears']
---> pop last item from list
['apples', 'oranges', 'grapes', 'bananas', 'melons', 'peaches']
---> remove peaches from list
['apples', 'oranges', 'grapes', 'bananas', 'melons']
---> remove first element from list
['oranges', 'grapes', 'bananas', 'melons']
---> list sort
['bananas', 'grapes', 'melons', 'oranges']
---> adding elements to a list
['dates', 'bananas', 'grapes', 'melons', 'oranges', 'plums', 'cherries']
---> join list to a string
dates bananas grapes melons oranges plums cherries

"""

if __name__ == "__main__":
    stra = "apples oranges grapes bananas melons peaches pears"
    strb = None
    lena = 7
    lsta = None
    token = " "

    print("---> original string")
    print(stra)
    print("---> string split into a list")
    lsta = stra.split()
    print(lsta)
    lsta.pop()
    print("---> pop last item from list")
    print(lsta)
    lsta.remove("peaches")
    print("---> remove peaches from list")
    print(lsta)
    del lsta[0]
    print("---> remove first element from list")
    print(lsta)
    print("---> list sort")
    lsta.sort()
    print(lsta)
    print("---> adding elements to a list")
    lsta.append("plums")
    lsta.extend(["cherries"])
    lsta.insert(0,"dates")
    print(lsta)
    print("---> join list to a string")
    strb = token.join(lsta)
    print(strb)
